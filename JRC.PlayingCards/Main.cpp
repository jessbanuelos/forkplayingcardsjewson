
// Playing Cards
// Jason Clemons
// Part 2 by James Jewson

#include <iostream>
#include <conio.h>

using namespace std;

enum class Suit {
	Hearts,
	Diamonds,
	Spades,
	Clubs
};

enum class Rank {
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card {
	Suit Suit;
	Rank Rank;
};


void PrintCard(Card card) 
{
	string suit; 
	string rank;
	switch (card.Suit) {
		case Suit::Clubs:
			suit = "Clubs";
			break;
		case Suit::Diamonds:
			suit = "Diamonds";
			break;
		case Suit::Hearts:
			suit = "Hearts";
			break;
		default:
			suit = "Spades";
			break;
	}
	
	switch (card.Rank) {
		case Rank::Two:
			rank = "Two";
			break;
		case Rank::Three:
			rank = "Three";
			break;
		case Rank::Four:
			rank = "Four";
			break;
		case Rank::Five:
			rank = "Five";
			break;
		case Rank::Six:
			rank = "Six";
			break;
		case Rank::Seven:
			rank = "Seven";
			break;
		case Rank::Eight:
			rank = "Eight";
			break;
		case Rank::Nine:
			rank = "Nine";
			break;
		case Rank::Ten:
			rank = "Ten";
		case Rank::Jack:
			rank = "Jack";
			break;
		case Rank::Queen:
			rank = "Queen";
			break;
		case Rank::King:
			rank = "King";
			break;
		default:
			rank = "Ace";
			break;
	}
	
	cout << "The " << rank << " of " << suit << "\n";

}


Card HighCard(Card card1, Card card2) 
{
	if (card1.Rank == card2.Rank) {
		//Returns nothing, because neither card wins. 
	}
	else if (card1.Rank > card2.Rank) {
		return card1;
	}
	else
	{
		return card2;
	}
	
}

int main()
{
	Card c1;
	c1.Rank = Rank::Two;
	c1.Suit = Suit::Clubs;

	Card c2;
	c2.Rank = Rank::Five;
	c2.Suit = Suit::Diamonds;

	PrintCard(c1);
	HighCard(c1, c2);

	//Printed out HighCard winner, commented out for now.
	//PrintCard(HighCard(c1, c2));


	(void)_getch();
	return 0;
}
